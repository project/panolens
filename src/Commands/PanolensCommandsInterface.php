<?php

namespace Drupal\panolens\Commands;

use Drupal\Core\File\FileSystemInterface;

/**
 * Provides a drush commands interface.
 */
interface PanolensCommandsInterface {

  /**
   * Constructs a new Panolens Drush command.
   *
   * @param \Drupal\Core\File\FileSystemInterface $file_system
   *   File system.
   */
  public function __construct(FileSystemInterface $file_system);

}
