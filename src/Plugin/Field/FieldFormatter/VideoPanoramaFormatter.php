<?php

namespace Drupal\panolens\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\File\FileUrlGeneratorInterface;
use Drupal\file\Plugin\Field\FieldFormatter\FileVideoFormatter;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the 'Video Panorama' formatter.
 *
 * @FieldFormatter(
 *   id = "panolens_video_panorama",
 *   label = @Translation("Video Panorama"),
 *   description = @Translation("Display the file using the Panolens.js library."),
 *   field_types = {
 *     "file"
 *   }
 * )
 */
class VideoPanoramaFormatter extends FileVideoFormatter implements VideoPanoramaFormatterInterface {

  /**
   * The file URL generator.
   *
   * @var \Drupal\Core\File\FileUrlGeneratorInterface
   */
  protected $fileUrlGenerator;

  /**
   * Constructs a ImagePanoramaFormatter object.
   *
   * @param string $plugin_id
   *   The plugin_id for the formatter.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The definition of the field to which the formatter is associated.
   * @param array $settings
   *   The formatter settings.
   * @param string $label
   *   The formatter label display setting.
   * @param string $view_mode
   *   The view mode.
   * @param array $third_party_settings
   *   Any third party settings.
   * @param \Drupal\Core\File\FileUrlGeneratorInterface $file_url_generator
   *   The file URL generator.
   */
  public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, $label, $view_mode, array $third_party_settings, FileUrlGeneratorInterface $file_url_generator) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $label, $view_mode, $third_party_settings);
    $this->fileUrlGenerator = $file_url_generator;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['label'],
      $configuration['view_mode'],
      $configuration['third_party_settings'],
      $container->get('file_url_generator')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];
    $source_files = $this->getSourceFiles($items, $langcode);

    // Early opt-out if the field is empty.
    if (empty($source_files)) {
      return $elements;
    }

    foreach ($source_files as $delta => $files) {
      $file = array_pop($files)['file'];
      $cache_contexts = [];

      $file_uri = $this->fileUrlGenerator->generateString($file->getFileUri());

      $cache_contexts[] = 'url.site';
      $cache_tags = $file->getCacheTags();

      $elements[$delta] = [
        "#prefix" => '<div class="panolens-container">',
        '#markup' => '<div class="panolens-item" data-format="video-panorama" data-url="' . $file_uri . '"></div>',
        "#suffix" => '</div>',
        '#attached' => [
          'library' => [
            'panolens/video-panorama',
          ],
        ],
        '#cache' => [
          'tags' => $cache_tags,
          'contexts' => $cache_contexts,
        ],
      ];
    }
    return $elements;
  }

}
