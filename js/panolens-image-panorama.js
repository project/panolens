(function ($, Drupal) {

  "use strict";

  Drupal.behaviors.panolensImagePanorama = {
    attach: function (context, settings) {
      once('panolensImagePanorama', 'body', context).forEach(function (element) {

        let panorama = [];
        let viewer = [];

        $('.panolens-item[data-format="image-panorama"]', context).each(function (index) {
          $(this)
            // Adjust parent elements to become full-width.
            .parent().parent().parent().css('width', '100%');

          let url = $(this).data('url');

          panorama[index] = new PANOLENS.ImagePanorama(url);
          viewer[index] = new PANOLENS.Viewer({container: this});
          viewer[index].add(panorama[index]);
        });

      });
    }
  };

})(jQuery, Drupal);
