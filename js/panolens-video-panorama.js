(function ($, Drupal) {

  "use strict";

  Drupal.behaviors.panolensVideoPanorama = {
    attach: function (context, settings) {
      once('panolensVideoPanorama', 'body', context).forEach(function (element) {

        let panorama = [];
        let viewer = [];

        $('.panolens-item[data-format="video-panorama"]', context).each(function (index) {
          $(this)
            // Adjust parent elements to become full-width.
            .parent().parent().parent().css('width', '100%');

          let url = $(this).data('url');

          panorama[index] = new PANOLENS.VideoPanorama(url);
          viewer[index] = new PANOLENS.Viewer({container: this});
          viewer[index].add(panorama[index]);
        });

      });
    }
  };

})(jQuery, Drupal);
